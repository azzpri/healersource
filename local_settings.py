import os
import sys

from settings import *

DATABASE_NAME = "hs_database"
DATABASE_USER = "healersource"
DATABASE_PASSWORD = "4567"
DATABASE_ENGINE = "django.contrib.gis.db.backends.postgis"
DATABASE_HOST = '127.0.0.1'
DATABASE_PORT = ''

DATABASES = {
	"default": {
		"ENGINE": "django.contrib.gis.db.backends.postgis", # Add "postgresql_psycopg2", "postgresql", "mysql", "sqlite3" or "oracle".
		"NAME": "hs_database",                       # Or path to database file if using sqlite3.
		"USER": "healersource",                             # Not used with sqlite3.
		"PASSWORD": "4567",                         # Not used with sqlite3.
		"HOST": "127.0.0.1",                             # Set to empty string for localhost. Not used with sqlite3.
		"PORT": "",                             # Set to empty string for default. Not used with sqlite3.
	}
}

INSTALLED_APPS = ["django_comments" if app == "django.contrib.comments" else app for app in INSTALLED_APPS]
INSTALLED_APPS += ["django_extensions"]

ROOT_URLCONF = 'urls'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

GEOS_LIBRARY_PATH = '/usr/local/lib/libgeos_c.so'
#GEOIP_LIBRARY_PATH = '/opt/local/lib/libGeoIP.dylib'

ENCRYPTED_FIELDS_KEYDIR = '/home/vagrant/fieldkeys'


STRIPE_SECRET_KEY = 'sk_test_sqzVOMcqaqM3p1Gh34el2sJX'
STRIPE_PUBLIC_KEY = 'pk_test_y9HQeezQY1rPRqSUX4Gt2D5o'
STRIPE_APP_CLIENT_ID = 'ca_5nWEWiXx340qQElfoWwC5NcT9gxoyKAx'

PAYPAL_TEST = True
PAYPAL_RECEIVER_EMAIL = 'seller@desecho.net'

GOOGLE_OAUTH_REDIRECT_URL = "http://change-to-ngrok/oauth2callback/google/" 
GOOGLE_CLIENT_ID = ""
GOOGLE_CLIENT_SECRET = ""

FACEBOOK_KEY = ""
FACEBOOK_SECRET = ""
FACEBOOK_DEV = True

SOCIAL_AUTH_FACEBOOK_APP_KEY = ''
SOCIAL_AUTH_FACEBOOK_APP_SECRET = ''
SOCIAL_AUTH_FACEBOOK_SCOPE = [
	'user_website', 'email', 'user_friends', 'user_location', 'user_photos']


# os.environ['DJANGO_SETTINGS_MODULE'] = "healersource.settings"

#from django.core.management import setup_environ
#setup_environ(settings)

path = '/healersource/healersource'
if path not in sys.path:
	sys.path.append(path)

COPY_ADMIN_REMINDER = True
COPY_ADMIN_CONFIRMATION = True

EMAIL_ON_SIGNUP = []

RUNNING_LOCAL = True

ALLOWED_HOSTS = ['*',]

CACHES = {
	'default': {
		'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
	}
}

DEBUG = True

DEFAULT_HTTP_PROTOCOL = 'http'

# if DEBUG:
#     INSTALLED_APPS.append("debug_toolbar")
#     INTERNAL_IPS = ['127.0.0.1', '10.0.2.2']
#     DAJAXICE_XMLHTTPREQUEST_JS_IMPORT = False


# disable migrations for testing
if len(sys.argv)>1 and sys.argv[1] == 'test':
	MIGRATION_MODULES = {
	'django.contrib.auth': 'django.contrib.auth.migrations_not_used_in_tests',
	'django.contrib.contenttypes': 'django.contrib.contenttypes.migrations_not_used_in_tests',
	'django.contrib.redirects': 'django.contrib.redirects.migrations_not_used_in_tests',
	'django.contrib.sessions': 'django.contrib.sessions.migrations_not_used_in_tests',
	'django.contrib.sites': 'django.contrib.sites.migrations_not_used_in_tests',
	'django.contrib.messages': 'django.contrib.messages.migrations_not_used_in_tests',
	'django.contrib.humanize': 'django.contrib.humanize.migrations_not_used_in_tests',
	'markup_deprecated': 'markup_deprecated.migrations_not_used_in_tests',
	'django.contrib.comments': 'django.contrib.comments.migrations_not_used_in_tests',
	'django.contrib.sitemaps': 'django.contrib.sitemaps.migrations_not_used_in_tests',
	'django.contrib.gis': 'django.contrib.gis.migrations_not_used_in_tests',
	'pinax.templatetags': 'pinax.templatetags.migrations_not_used_in_tests',
	'django.contrib.staticfiles': 'django.contrib.staticfiles.migrations_not_used_in_tests',
	'uni_form': 'uni_form.migrations_not_used_in_tests',
	'ajax_validation': 'ajax_validation.migrations_not_used_in_tests',
	'timezones': 'timezones.migrations_not_used_in_tests',
	'emailconfirmation': 'emailconfirmation.migrations_not_used_in_tests',
	'pagination': 'pagination.migrations_not_used_in_tests',
	'friends': 'friends.migrations_not_used_in_tests',
	'django_messages': 'django_messages.migrations_not_used_in_tests',
	'oembed': 'oembed.migrations_not_used_in_tests',
	'wakawaka': 'wakawaka.migrations_not_used_in_tests',
	'tagging': 'tagging.migrations_not_used_in_tests',
	'avatar': 'avatar.migrations_not_used_in_tests',
	'django_sorting': 'django_sorting.migrations_not_used_in_tests',
	'django_markup': 'django_markup.migrations_not_used_in_tests',
	'tagging_ext': 'tagging_ext.migrations_not_used_in_tests',
	'oauth_access': 'oauth_access.migrations_not_used_in_tests',
	'captcha': 'captcha.migrations_not_used_in_tests',
	'pinax.apps.account': 'pinax.apps.account.migrations_not_used_in_tests',
	'pinax.apps.waitinglist': 'pinax.apps.waitinglist.migrations_not_used_in_tests',
	'about': 'about.migrations_not_used_in_tests',
	'dajaxice': 'dajaxice.migrations_not_used_in_tests',
	'dajax': 'dajax.migrations_not_used_in_tests',
	'clients': 'clients.migrations_not_used_in_tests',
	'healers': 'healers.migrations_not_used_in_tests',
	'schedule': 'schedule.migrations_not_used_in_tests',
	'friends_hs': 'friends_hs.migrations_not_used_in_tests',
	'friends_app': 'friends_app.migrations_not_used_in_tests',
	'feedback': 'feedback.migrations_not_used_in_tests',
	'sync_hs': 'sync_hs.migrations_not_used_in_tests',
	'modality': 'modality.migrations_not_used_in_tests',
	'contacts_hs': 'contacts_hs.migrations_not_used_in_tests',
	'client_notes': 'client_notes.migrations_not_used_in_tests',
	'blog_hs': 'blog_hs.migrations_not_used_in_tests',
	'search': 'search.migrations_not_used_in_tests',
	'mezzanine.core': 'mezzanine.core.migrations_not_used_in_tests',
	# 'mezzanine.generic': 'mezzanine.generic.migrations_not_used_in_tests',
	'mezzanine.blog': 'mezzanine.blog.migrations_not_used_in_tests',
	'review': 'review.migrations_not_used_in_tests',
	'errors_hs': 'errors_hs.migrations_not_used_in_tests',
	'healing_requests': 'healing_requests.migrations_not_used_in_tests',
	'audit': 'audit.migrations_not_used_in_tests',
	'send_healing': 'send_healing.migrations_not_used_in_tests',
	'metron': 'metron.migrations_not_used_in_tests',
	'account_hs': 'account_hs.migrations_not_used_in_tests',
	'compressor': 'compressor.migrations_not_used_in_tests',
	'intake_forms': 'intake_forms.migrations_not_used_in_tests',
	'django_select2': 'django_select2.migrations_not_used_in_tests',
	'payments': 'payments.migrations_not_used_in_tests',
	'util': 'util.migrations_not_used_in_tests',
	'endless_pagination': 'endless_pagination.migrations_not_used_in_tests',
	'messages_hs': 'messages_hs.migrations_not_used_in_tests',
	'dashboard': 'dashboard.migrations_not_used_in_tests',
	'django_nose': 'django_nose.migrations_not_used_in_tests',
	# 'gratitude_stream': 'gratitude_stream.migrations_not_used_in_tests',
	'provider': 'provider.migrations_not_used_in_tests',
	'provider.oauth2': 'provider.oauth2.migrations_not_used_in_tests',
	'rest_framework': 'rest_framework.migrations_not_used_in_tests',
	'corsheaders': 'corsheaders.migrations_not_used_in_tests',
	'oauth2_hs': 'oauth2_hs.migrations_not_used_in_tests',
	'events': 'events.migrations_not_used_in_tests',
	'ambassador': 'ambassador.migrations_not_used_in_tests'
	}

